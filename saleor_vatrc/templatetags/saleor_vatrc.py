"""Custom tags for various VAT-related utilities."""
from django import template

import saleor_vatrc.plugin

register = template.Library()


@register.simple_tag(takes_context=True)
def get_value_from_metadata(context, obj, key: str) -> bool:
    """Retrieve a value from object's metadata."""
    return obj.metadata_storage.get_value_from_metadata(key)


@register.simple_tag(takes_context=True)
def is_vatrc_applicable(context, order) -> bool:
    """Return True if reverse charged VAT is applicable to a given order."""
    vatin_key = saleor_vatrc.plugin.VatReverseCharge.META_VATIN_VALIDATED_KEY
    vatin = order.metadata_storage.get_value_from_metadata(vatin_key)
    if not vatin:
        return False
    billing_country = order.billing_address.country
    if not order.channel.tax_configuration:
        return False
    if billing_country.code == order.channel.default_country:
        return False
    return True


# FIXME(fsiddi) _get_taxes_for_country is not available anymore
# @register.simple_tag(takes_context=True)
# def is_vat_applicable(context, order):
#     """Return True if VAT is applicable to a given order."""
#     billing_country = order.billing_address.country
#     if not vat_plugin._get_taxes_for_country(billing_country):
#         return False
#     return True
